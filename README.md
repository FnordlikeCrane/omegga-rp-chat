# Omegga RP Chat

A Work-in-Progress, New-and-Improved version of the [old RP-Chat](https://gitlab.com/FnordlikeCrane/omegga-rp-chat-old) that was written in Javascript.

Plugin for [omegga](https://github.com/brickadia-community/omegga).


## Install

**REMOVE THE OLD RP-CHAT BEFORE TRYING TO INSTALL THIS VERSION** 

`omegga install gl:FnordlikeCrane/omegga-rp-chat`

------------------
## Commands List

### `/alias <NAME>`

Set an alias for yourself, where `<NAME>` is the alias you which to go by (***remove** pointy brackets unless you want your alias to include them*).

This displays a message to the whole server by deafult.

Not including `<NAME>` will result in no alias being set and an error message will be displayed.

Your player's alias is persistent and will remain the way it was last time you entered the server.

**Alias-Banned players may not use this command.**


### `/alias:rm [target]`

If you do not include a target player, or if the target is your own name, this will remove your own alias.

If you are an authorized user, include a target player's username, as close as it can be, directly after the command (*Omegga will auto-find the closest match*). If this optional parameter is included, then this player will have their alias removed.

Do not inlude square brackets for `[target]` unless your target's username includes them.


### `/alias:color <#HEXCODE>`

Set your alias' color to the provided **7-Character** hexcode input. 

Input must be provided. **Do not** include the pointy brackets in the input.

The first character must be a `#` otherwise this command will not work. 6 charcters must follow the `#` symbol, otherwise this command will not work. The **six characters** to follow the `#` **must only include Numbers and Letters A-F**, otherwise this command will not work. Any non-correct input will provide an error message and result in no change.

If you don't know how hexcodes work, or you do not know how the alphanumeric input coresponds to the resulting color, try this nifty [Colorpicker](https://duckduckgo.com/?q=colorpicker&ia=answer).

**Alias-Banned players may not use this command.**


### `/alias:ls`

Receive a list of all players registered with and alias. This provides the username of the player, the alias of the player, and the hexcode registered with the player.


### `/alias:clearall`

Clear all aliases in the server.

This command also resets all player's RP Modes to **Out of Context**.

You must be authorized to use this command (*server host is authorized by default*).


### `/alias:ban <target>`  **(BUGGED COMMAND)**

Ban a target player from using aliases and by extesnion, RPing. Provide the name of the player you wish to ban, as close as you can (*Omegga will auto-find the closest match*).

You must be authorized to use this command (*server host is authorized by default*).

A banned player may not use certain commands: `/rp`, `/alias`, `/alias:color`, and certain functions of `//`

**Do not** include the pointy-brackets in the target's name unless they are a part of the player's username in-game.

You must be authorized to use this command (*server host is authorized by default*).

**This command is currently bugged**, but will be fixed soon! (*Bans with the command only last temporarily*)

Use the Web-Interface to ban and un-ban players for now.


### `/alias:unban <target>` **(BUGGED COMMAND)**

Un-Ban a banned player from using aliases. Provide the name of the player you wish to un-ban, as close as you can (*Omegga will auto-find the closest match*).

You must be authorized to use this command (*server host is authorized by default*). The authorization for this command is shared with the command `/alias:ban`.

**Do not** include the pointy-brackets in the target's name unless they are a part of the player's username in-game.

You must be authorized to use this command (*server host is authorized by default*).

**This command is currently bugged**, but will be fixed soon!

Use the Web-Interface to ban and un-ban players for now.

### `/rp`

Enter your player into **Role Playing Mode**. In RP Mode, further messages will be sent as your alias.

Your player's chat mode is persistent and will remain the way it was last time you entered the server.

If you are in RP Mode, chats you send will display to everyone but you as your alias. To you, your actual username will be displayed and a second chat will send to **only you** letting you know your current alias and that the message was sent in RP Mode.

**Alias-Banned players may not use this command.**


### `/ooc`

Enter your player into **Out of Context Mode**. Your player enters the server in OOC mode by default, and messages sent in OOC only show your acutal player's username. OOC Mode messages do no display your alias.

Your player's chat mode is persistent and will remain the way it was last time you entered the server.

If you use the `//` command in OOC mode, your message is sent in RP Mode, unless you have been banned from aliasing.


### `// <MESSAGE>` *(Inverted Mode Chat)*

This command will send the provided message in the opposite mode your character is currently in.

After the two *forward slashes* (`//`), you **must provide a space followed by the message**. **If your input doesn't include the space between `//` and the message, it will not work!**

To elaborate, if you have entered `/rp` and your character is in **Role Playing Mode**, the message is sent in **Out of Context Mode**.

And vice-versa, if you have entered `/ooc` or are otherise in **Out of Context Mode** *(the default mode)* then, your message is sent in **Role Playing Mode**.

This is an important distinction because in OOC mode, the `//` works differently from simply sending a chat after using `/rp`.

A regular chat in RP Mode will display your actual username to you, but an OOC Mode chat with `//` will not display your actual username. Instead it will send the message as your alias **to all**, ***including you***, and it **will not send a second message** to you to let you know.

Alias-Banned players **may not use this command** if they are **in OOC Mode**.


------------------
## Roadmap & Plans

**Fix bugs!!**

Actually implent role features

Add a command where the server host may store aliases to a local file to save for later

Add profanity filter for chat as well as setting aliases

Add commands for authorized users to manually set other players' aliases and alias colors

Add a config option to make the command, `/alias:ls`, only function for authorized users

Add config options to make alias switching not include a public message

Add config options to make alias switching have a cooldown

Add config options for displaying the authenticity of an alias
(*Distinguish a player with an alias the same as another player's actual username*)

Add distanced chat functions similar to the original version as an optional feature
