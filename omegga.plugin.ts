import OmeggaPlugin, { OL, PS, PC, OmeggaPlayer } from 'omegga';

const { chat: { sanitize }, } = OMEGGA_UTIL;

type PlayerRpMode = [string, boolean];
type PlayerAlias = {player_alias: string, alias_color?: string};

type Config = { "Banned from Aliases": OmeggaPlayer[] };
type Storage = {
	"Player Aliases": Record<string, PlayerAlias>,
	"Player RP Modes": Record<string, boolean>,
};

// const ORANGE: Readonly<string> = "fc9905";
// const GREY: Readonly<string> = "aaaaaa";
// const RED: Readonly<string> = "ff5555";
// const GREEN: Readonly<string> = "55ff55";


function atos(...a: string[]): string {
	let str: string = "";
	for (const s of a) {
		str = str + sanitize(s) + " ";
	}
	return str.trim();
}

export default class Plugin implements OmeggaPlugin<Config, Storage> {
	omegga: OL;
	config: PC<Config>;
	store: PS<Storage>;

	constructor(omegga: OL, config: PC<Config>, store: PS<Storage>) {
		this.omegga = omegga;
		this.config = config;

		if ( store.get("Player Aliases") === null || store.get("Player Aliases") === undefined ) {
			store.set("Player Aliases", {});
		}
		if ( store.get("Player RP Modes") === null || store.get("Player RP Modes") === undefined ) {
			store.set("Player RP Modes", {});
		}
		
		this.store = store;
	}

	checkPlayerAliasBanned(person: string): boolean {
		const target = this.omegga.findPlayerByName(person)
		for (const player of this.config['Banned from Aliases']) {
			if (player === target) {
				return true;
			}
		}
		return false;
	}

	checkPlayerIsHost(person: string): boolean {
		return this.omegga.findPlayerByName(person).id === this.omegga.getHostId()
	}

	checkRmAliasAuth(person: string): boolean {
		const target = this.omegga.findPlayerByName(person);
		for (const player of this.config['Authorized to Clear Target Alias']) {
			if ( player === target ) {
				return true;
			}
		}
		return false;
	}

	checkClearAllAliasAuth(person: string): boolean {
		const target = this.omegga.findPlayerByName(person);
		for (const player of this.config['Authorized to Clear All Aliases']) {
			if ( player=== target ) {
				return true;
			}
		}
		return false;
	}

	async getRpMode(player_name: string): Promise<PlayerRpMode> {
		let rp_modes = await this.store.get("Player RP Modes");
		if (rp_modes === undefined) {
			rp_modes = {};
		}
		if (rp_modes.hasOwnProperty(player_name)) {
			return [player_name, rp_modes[player_name]];
		} else {
			return ["", false];
		}
	}

	async setRpMode(person: string, mode: boolean) {
		let modes = await this.store.get("Player RP Modes");
		if ( modes === undefined ) {
			modes = {};
		}
		modes[person] = mode;
		this.store.set("Player RP Modes", modes);
	}

	async rpChat(person: string, ...args: string[]) {
		let rp_mode: PlayerRpMode = await this.getRpMode(person);
		let alias: PlayerAlias;
		let name_str: string;
		const player_name_color = this.omegga.getPlayer(person).getNameColor();
		if ( this.checkPlayerAliasBanned(person) === true && rp_mode[1] === true ) {			
			this.omegga.whisper(person, `"<color=\\"fc9905\\">Alert</>:" You are currently banned from using an alias, so you have been switched to "<color=\\"aaaaaa\\">OOC</>" chat mode.`);
			await this.setRpMode(person, false);
			rp_mode = await this.getRpMode(person);
		} 
		if ( rp_mode[0] === "" )  {
			// player is not registered as either RP chatting or OOC chatting, so register them
			this.setRpMode(person, false);
			name_str = `"<size=\\"0\\">!</><b><color=\\"${player_name_color}\\">${sanitize(person + "").trim()}</></>:"`.trim();
		} else if ( rp_mode[1] === true && rp_mode[0] === person ) {
			// The player is trying to RP Chat
			let all_aliases = await this.store.get("Player Aliases");
			if ( all_aliases === undefined ) {
				all_aliases = {};
			} else if ( all_aliases.hasOwnProperty(person) && all_aliases[person]['player_alias'] !== "" ) {
				// The player has an alias for RP mode
				alias = all_aliases[person];
				name_str = `"<size=\\"0\\">!</><b><color=\\"${alias['alias_color']}\\">${alias['player_alias']}</></>:"`.trim();
			} else if ( all_aliases.hasOwnProperty(person) && all_aliases[person]['player_alias'] === "" ) {
				// The player has set a color to their alias but has no alias name
				name_str = `"<size=\\"0\\">!</><b><color=\\"${player_name_color}\\">${sanitize(person+"").trim()}</></>:"`.trim();
			} else {
				// The player has no alias, send a message with their actual player name, but warn them
				name_str = `"<size=\\"0\\">!</><b><color=\\"${player_name_color}\\">${sanitize(person+"").trim()}</></>:"`.trim();
				this.omegga.whisper(person, `"<color=\\"aaaaaa\\">Warning</>:" You are trying to RP chat without an alias!`);
			}
		} else {
			// The player has RP Mode set to false, so they are not trying to RP Chat
			name_str = `"<size=\\"0\\">!</><b><color=\\"${player_name_color}\\">${sanitize(person + "").trim()}</></>:"`.trim();
		}

		const message = sanitize(args + "").trim();

		for (const player of this.omegga.players) {
			if (player.name === person) {
				if ( alias !== undefined )  {
					this.omegga.whisper(player, `"<color=\\"aaaaaa\\">[</> As <color=\\"${alias['alias_color']}\\">${alias['player_alias']}</> <color=\\"aaaaaa\\">]</>"`);
				}
				continue;
			}
			this.omegga.whisper(player, `${name_str} ${message}`.trim());
		}
	}

	async registerAliasColor(person: string, hex_color: string) {
		if ( this.checkPlayerAliasBanned(person) === true ) {
			this.omegga.whisper(person, `"<color=\\"ff5555\\">Error</>:"You are currently banned from using an alias.`);
			return
		}

		if ( hex_color.length > 7 || hex_color.length < 7 ) {
			// Non 6-length input
			this.omegga.whisper(person, `"<color=\\"ff5555\\">Error</>:" Input must be exactly 7 characters long and start with a #.`);
		} else {
			const validate: RegExp = /^#([0-9a-f]{3}){1,2}$/i;
			if ( validate.test(hex_color) === false ) {
				// Invalid input
				this.omegga.whisper(person, `"<color=\\"ff5555\\">Error</>:" Input must be a valid hexidecimal code.`);
				this.omegga.whisper(person, `"<code>The code must start with # and only contain numbers and letters A-F</>"`);
			} else {
				hex_color = hex_color.replace("#", "");

				// Valid hex code entered, but must check to see if an alias exists
				let alias: PlayerAlias;
				let all_aliases = await this.store.get("Player Aliases");
				if ( all_aliases === undefined ) {
					all_aliases = {};
					all_aliases[person] = alias;
				}

				if ( all_aliases.hasOwnProperty(person) === false ) {
					alias = {'player_alias': "", 'alias_color': ""};
					all_aliases[person] = alias;
				}
				if ( all_aliases[person]['player_alias'] !== "" && all_aliases[person]['player_alias'] !== undefined ) {
					const alias_name: string = all_aliases[person]['player_alias'];
					alias = {'player_alias': alias_name, 'alias_color': hex_color};
					all_aliases[person] = alias;
					// Successful recolor of alias
					this.store.set("Player Aliases", all_aliases);		

					this.omegga.whisper(person, `"<color=\\"55ff55\\">Success</>:" Your alias color has changed to "<color=\\"${hex_color}\\">${alias['player_alias']}</>!"`);
				} else {
					alias = {'alias_color': hex_color, 'player_alias': ""};
					all_aliases[person] = alias;
					// Although the player has no alias, it has been recolored!
					await this.store.set("Player Aliases", all_aliases);

					this.omegga.whisper(person, `"<color=\\"aaaaaa\\">Warning</>:" You do not have an alias, but your alias color has changed!`);
					const text: string = `"<color=\\"${hex_color}\\">${"#" + hex_color.toLocaleLowerCase()}</>!"`;
					this.omegga.whisper(person, `"<color=\\"55ff55\\">Success</>:" Your alias color has changed to ${text}`);
				}
			}
		}
	}

	async checkAliasExists(person: string, alias: string): Promise<boolean> {
		const all_aliases = await this.store.get("Player Aliases");
		if ( all_aliases === undefined || Object.keys(all_aliases).length === 0 ) {
			return false;
		}
		for ( const other_player in all_aliases) {
			if ( other_player === person ) {
				continue;
			}
			const other_alias_obj = all_aliases[other_player];
			if ( other_alias_obj === undefined ) {
				continue;
			}
			const other_alias = other_alias_obj['player_alias'];
			if ( other_alias === alias ) {
				return true;
			}
		}
		return false;
	}

	async registerPlayerAlias(person: string, ...trying: string[]) {
		if (trying.length === 0) {
			this.omegga.whisper(person, `"<color=\\"ff5555\\">Error</>:" You must enter an alias you wish to go by`);
			console.log("After all edits to the store:", await this.store.get("Player Aliases"));
			return;
		}
		if ( this.checkPlayerAliasBanned(person) === true ) {
			this.omegga.whisper(person, `"<color=\\"ff5555\\">Error</>:" You are currently banned from using an alias.`);
			this.setRpMode(person, false);
			return;
		}

		let all_aliases: Record<string, PlayerAlias> = await this.store.get("Player Aliases");
		const player_obj = this.omegga.getPlayer(person);

		const alias = atos(...trying);
		if ( all_aliases !== undefined && all_aliases.hasOwnProperty(person) ) {
			let current_alias = all_aliases[person]['player_alias'];			
			let current_color = all_aliases[person]['alias_color'];
			if ( current_alias === undefined ) {
				current_alias = "";
			}
			if ( current_color === undefined ) {
				current_color = player_obj.getNameColor();
			}
			if (current_alias === alias) {
				this.omegga.whisper(person, `"<color=\\"ff5555\\">Error</>:" This is already your alias!`);
			} else if ( await this.checkAliasExists(person, alias) === true ) { // Check to make sure no one else goes by the same alias
				// Someone else has this alias
				this.omegga.whisper(person, `"<color=\\"ff5555}\\">Error</>:" Someone else has this alias!`);
			} else {
				// Successs!
				const color = ( all_aliases[person]['alias_color'] === undefined ) ? player_obj.getNameColor() : all_aliases[person]['alias_color'];
				const old_alias = (current_alias === "") ? person: current_alias;
				all_aliases[person] = {'player_alias': alias, 'alias_color': color};
				this.store.set("Player Aliases", all_aliases);
				console.log(`${person} is no known as ${alias} in the MERP!`);
				this.omegga.whisper(person, `Your old alias of "<color=\\"aaaaaa\\">${old_alias}</>" has been changed to "<color=\\"${color}\\">${alias}</>!"`);
				const name = `"<size=\\"0\\">!</><b><color=\\"${player_obj.getNameColor()}\\">${sanitize(person + "").trim()}</></>"`.trim();
				this.omegga.broadcast(`${name} is now known as "<color=\\"${color}\\">${alias}</>" in the RP!`);
			}
		} else {
			const color = player_obj.getNameColor();
			all_aliases[person] = {'player_alias': alias, 'alias_color': color};
			this.store.set("Player Aliases", all_aliases);
			const name = `"<size=\\"0\\">!</><b><color=\\"${color}\\">${sanitize(person + "").trim()}</></>"`.trim();
			this.omegga.broadcast(`${name} is now known as "<color=\\"${color}\\">${alias}</>" in the RP!`);
		}
	}

	async listAllAliases(person: string) {
		this.omegga.whisper(person, `"<color=\\"55ff55\\">List of all player aliases</>:"`);
		const all_aliases: Record<string, PlayerAlias> = await this.store.get("Player Aliases");
		console.log("All server aliases: ", all_aliases);
		if ( all_aliases === undefined || Object.keys(all_aliases).length === 0) {
			this.omegga.whisper(person, `"<color=\\"aaaaaa\\">There are currently no player aliases on this server</>"`);
			if ( all_aliases === undefined ) {
				this.store.set("Player Aliases", {});
			}
		} else {
			for (const [player_name, alias] of Object.entries(all_aliases)) {
				let player_obj = this.omegga.findPlayerByName(player_name);
				let color: string;
				if ( player_obj === undefined ) {
					color = "aaaaaa";
				} else {
					color = player_obj.getNameColor();
				}
				const name = `"<size=\\"0\\">!</><b><color=\\"${color}\\">${sanitize(player_name + "").trim()}</></>"`.trim();
				this.omegga.whisper(person, `${name} has alias "<color=\\"aaaaaa\\">${alias['player_alias']}</> with colorcode <color=\\"${alias['alias_color']}\\">#${alias['alias_color']}</>"`);
			}
		}
	}

	async rpInvertChat(person: string, ...message: string[]) {
		const player_mode = await this.getRpMode(person);
		if ( player_mode[0] === "" && player_mode[1] === false ) {
			await this.setRpMode(person, true);
			await this.rpChat(person, ...message);
			await this.setRpMode(person, false);
		} else {
			const current_mode: boolean = player_mode[1];
			if ( current_mode === true ) {
				// Player is attempting to OOC chat, meaning we have to print their own message to themselves as themselves
				const player_name_color = this.omegga.findPlayerByName(person).getNameColor();
				const name = `"<size=\\"0\\">!</><b><color=\\"${player_name_color}\\">${sanitize(person + "").trim()}</></>:"`.trim();
				const sanitized_message = atos(...message);
				this.omegga.whisper(person, `${name} ${sanitized_message.trim()}`);

				await this.setRpMode(person, false);
				await this.rpChat(person, ...message);
				await this.setRpMode(person, true);
			} else {
				if ( this.checkPlayerAliasBanned(person) == true ) {
					await this.setRpMode(person, false);
					const name = `"<size=\\"0\\">!</><b><color=\\"${this.omegga.getPlayer(person).getNameColor()}\\">${sanitize(person + "").trim()}</></>:"`.trim();
					this.omegga.broadcast(`${name} ${atos(...message)}`);
					this.omegga.whisper(person, `"<color=\\"fc9905\\">Alert</>:" You are currently banned from using an alias, so you have been switched to "<color=\\"aaaaaa\\">OOC</>" chat mode.`);
				} else {
					// The player is attempting to RP chat, meaning we have to print their message to themselves as their alias
					const all_aliases = await this.store.get("Player Aliases");
					let alias: PlayerAlias;
					let name: string;
					if ( all_aliases.hasOwnProperty(person) && all_aliases[person]['player_alias'] !== "" ) {
						// The player has an alias for RP mode
						alias = all_aliases[person];
						name = `"<size=\\"0\\">!</><b><color=\\"${alias['alias_color']}\\">${alias['player_alias']}</></>:"`.trim();
					} else {
						// The player has no alias, send a message with their actual player name, but warn them
						const color = this.omegga.findPlayerByName(person).getNameColor();
						name = `"<size=\\"0\\">!</><b><color=\\"${color}\\">${sanitize(person + "").trim()}</></>:"`.trim();
						this.omegga.whisper(person, `"<color=\\"aaaaaa\\">Warning</>:" You are trying to RP chat without an alias!`);
					}
					const sanitized_message = atos(...message);
					for (const player of this.omegga.players) {
						this.omegga.whisper(player, `${name} ${sanitized_message.trim()}`);
					}
				}
			}
		}
	}

	async clearPlayerAlias(person: string, target: string) {
		person = sanitize(person+"").trim();
		if ( this.omegga.getPlayer(target) === undefined ) {
			this.omegga.whisper(person, `"<color=\\"ff5555\\">Error</>:" Target with name "<color=\\"aaaaaa\\">${target}</>" doesn't exist.`);
		} else {
			if ( this.checkPlayerIsHost(person) || this.omegga.getPlayer(person).id === this.omegga.getPlayer(target).id || this.checkRmAliasAuth(person) ) {
				let all_aliases = await this.store.get("Player Aliases");
				if ( all_aliases === undefined ) {
					all_aliases = {};
				}
				const target_id = this.omegga.getPlayer(target).id;
				
				if ( (all_aliases[target_id] === undefined || all_aliases[target_id]['alias'] === "") && (all_aliases[target_id]['color'] === undefined || all_aliases[target_id]['color'] === "") ) {
					this.omegga.whisper(person, `"<color=\\"ff5555\\">Error</>: <color=\\"aaaaaa\\">${target}</>" has no alias to remove.`);
				} else {
					all_aliases[target_id] = {'player_alias': "", 'alias_color': undefined};
					this.store.set("Player Aliases", all_aliases);
					
					if ( this.omegga.getPlayer(person).id === this.omegga.getPlayer(target).id ) {
						this.omegga.whisper(person, `"<color=\\"55ff55\\">Success</>:" You have cleared your alias!`);
					} else {
						this.omegga.whisper(person, `"<color=\\"55ff55\\">Success</>:" You have cleared the alias for ${target}!`);
						this.omegga.whisper(target, ``);
					}
					this.setRpMode(target, false);
				}
			} else {
				console.warn(`Invalid Authorization for "${person}" to clear alias of "${target}".`);
				this.omegga.whisper(person, `"<color=\\"ff5555\\">Error</>:" You do not have authorization to clear this person's alias.`)
			}
		}
	}

	async banPlayerFromAliasing(person: string, target: string) {
		if ( this.checkPlayerIsHost(person) === false ) {
			this.omegga.whisper(person, `"<color=\\"ff5555\\">Error</>:" You must be authorized to use this command.`);
		} else {
			let banned_players = this.config["Banned from Aliases"];
			const banning_player = this.omegga.findPlayerByName(target);
			if ( banned_players.includes(banning_player) === true ) {
				this.omegga.whisper(person, `"<color=\\"ff5555\\">Error</>:" This player is already banned.`);
			} else {
				banned_players.push(banning_player);
				this.omegga.whisper(person, `"<color=\\"55ff55\\">Success</>: <color=\\"${banning_player.getNameColor()}\\">${banning_player.name}</>" is now banned from using aliases.`);
			}
			console.log("Current list of bans: ", banned_players);
		}
	}

	async unBanPlayerFromAliasing(person: string, target: string) {
		if ( this.checkPlayerIsHost(person) == false ) {
			this.omegga.whisper(person, `"<color=\\"ff5555\\">Error</>:" You must be authorized to use this command.`);
		} else {
			let banned_players = this.config["Banned from Aliases"];
			const unbanning_player = this.omegga.findPlayerByName(target);
			if ( banned_players.includes(unbanning_player) === false ) {
				this.omegga.whisper(person, `"<color=\\"55ff55\\">Success</>: <color=\\"${unbanning_player.getNameColor()}\\">${target}</>" is not banned from using aliases.`);
			} else {
				const banned_index = banned_players.indexOf(unbanning_player);
				banned_players.splice(banned_index, 1);
				this.config["Banned from Aliases"] = banned_players;
				this.omegga.whisper(person, `"<color=\\"55ff55\\">Success</>: <color=\\"${unbanning_player.getNameColor()}\\">${target}</>" is now unbanned from using aliases.`);
			}
		}
	}

	async init() {
		this.omegga.on('cmd:rp', async (person: string) => {
			// The player is banned so they cannot set themselves to RP mode
			if ( this.config['Banned from Aliases'].includes(this.omegga.findPlayerByName(person)) === true ) {
				this.omegga.whisper(person, `"<color=\\"ff5555\\">Error</>:" You are banned from aliasing so you may not enter RP mode.`);
			} else {
				await this.setRpMode(person, true);
				this.omegga.whisper(person, `You are now in "<color=\\"ff5555\\">Role Play</>" chat mode`);
			}
		});

		this.omegga.on('cmd:/', async (person: string, ...message: string[]) => {
			if ( message !== undefined && atos(...message).length !== 0 && atos(...message) !== "" ) {
				await this.rpInvertChat(person, ...message);
			}
		});

		this.omegga.on('cmd:ooc', async (person: string) => {
			await this.setRpMode(person, false);
			this.omegga.whisper(person, `You are now in "<color=\\"aaaaaa\\">Out Of Context</>" chat mode`);
		});

		this.omegga.on('chat', async (person: string, ...args: string[]) => {
			await this.rpChat(person, ...args);
		});

		this.omegga.on('cmd:alias', async (person: string, ...args: string[]) => {
			await this.registerPlayerAlias(person, ...args);
		});

		this.omegga.on('cmd:alias:ls', async (person: string) => {
			await this.listAllAliases(person);
		});

		this.omegga.on('cmd:alias:clearall', async (person: string) => {
			if ( this.checkPlayerIsHost(person) === true || this.checkClearAllAliasAuth(person) === true ) {
				this.store.delete("Player Aliases");
				this.store.delete("Player RP Mode");
				this.store.set("Player RP Modes", {});
				this.store.set("Player Aliases", {});
				this.omegga.broadcast(`"<color=\\"fc9905\\">Alert</>:" The server host has reset all player aliases!`);
			} else {
				console.warn(`Invalid Authorization for "${person}" to clear all aliases.`);
			}
		});

		this.omegga.on('cmd:alias:color', async (person: string, hex_color: string) => {
			if ( hex_color === undefined || hex_color.trim() === "" ) {
				this.omegga.whisper(person, `"<color=\\"ff5555\\">Error</>:" You must provide some input`);
			}
			await this.registerAliasColor(person, hex_color);
		});

		this.omegga.on('cmd:alias:rm', async (person: string, ...target: string[]) => {
			if ( target === undefined || target.length === 0 ) {
				const true_target = sanitize(person+"");
				this.clearPlayerAlias(person, true_target);
			} else {
				const true_target = atos(...target);
				this.clearPlayerAlias(person, true_target);
			}
		});

		this.omegga.on('cmd:alias:ban', async (person: string, ...target: string[]) => {
			if ( target === undefined || atos(...target) === "" ) {
				this.omegga.whisper(person, `"<color=\\"ff5555\\">Error</>:" You must provide a name to ban`);
			} else {
				const  target_obj = this.omegga.findPlayerByName(atos(...target));
				if ( target_obj === undefined ) {
					this.omegga.whisper(person, `"<color=\\"ff5555\\">Error</>: No player with that name found"`);
				} else {
					const target_name = target_obj.name;
					await this.banPlayerFromAliasing(person, target_name);
				}
			}
		});

		this.omegga.on('cmd:alias:unban', async (person: string, ...target: string[]) => {
			if ( target === undefined || atos(...target) === "") {
				this.omegga.whisper(person, `"<color=\\"ff5555\\">Error</>:" You must provide a name to unban`);
			} else {
				const target_obj: OmeggaPlayer = this.omegga.findPlayerByName(atos(...target));
				if ( target_obj === undefined ) {
					this.omegga.whisper(person, `"<color=\\"ff5555\\">Error</>: No player with that name found"`);
				} else {
					const true_target: string = target_obj.name;
					await this.unBanPlayerFromAliasing(person, true_target);
				}
			}
		});

		return { registeredCommands: ['rp', 'ooc', 'alias', 'alias:clearall', 'alias:ls', '/', 'alias:color', 'alias:rm', 'alias:rmcolor', 'alias:ban', 'alias:unban'] };
	}

	async stop() {
		// Anything that needs to be cleaned up...
	}
}
